# nugraph

## Nubank Challenge

- First step:

  Your program should read from a file of collisions and provide a function that
  answers if two nodes belong to the same collision network. The file is
  formatted as follows: each line represents a collision. In each line, we have
  two node ids separated by a single space.

- Second step:

  Your program should be able to handle two types of operations now:

 1. Add new collision between two nodes;  
 2. Answer if two nodes belong tothe same collision network.

- Third step:

  Implement a RESTful web server with endpoints for each of the operations from
  second step.

## Resolving the problem

#### The very beggining
First, this test demanded a learning of the graph theory which I had never directly worked with since now. It was a great jorney of knowledge! Only with a deep understanding of the problem very clear, I was ready to start coding. At the very beggining, I used Javascript first to write the Breadth-First Search algorithm (the one I choose to solve the problem), because it was my comfort zone. I could implement the algorithm in a few minutes. But, as I picked *Clojure* to be my partner, the simple *BFS* algorithm solution became such a REALLY BIG challenge! And it was awesome! ;) 


#### The solution
The example provided (and the file *collisions.txt* as well) is a graph represented in many pairs of vertices. Firstly, I read the file line by line, inserting them to the empty graph as each line represented an edge. To make it better and more manageable, I manipulated these vertices into _edges, vertices and adjacencies_. Using a vector of adjacencies ``( :v1 v2 and :v2 :v1)`` to represent the graph,  I could apply the BFS algorithm to search for the collisions. 

BFS was chosen based on the structure of the graph: non-directional, not weightened 


## Running
To run the application is very easy with Leiningen (make sure it's in your PATH). In a terminal, type:

```sh
$ lein ring server-headless :port 3000
```

and a server will start (in the `3000` port).


### Examples
Here, just to demonstration, I assumed that the pair of vertices in the file are those provided in the instructions:

```
1 2 
2 3
1 4
5 6
6 7
```

- Getting all the collisions:
    
    ```curl 
    GET /collisions
    ```
    
    The response will be:

    ```json
    [
      [1,2,3,4], 
      [5,6,7]
    ]
    ```


- Adding a new collision:

    ```curl
    POST /collisions/add
    ```

    - To add a new collision, you need to pass the two vertices in a JSON body with the parameters:
    
    | PARAMETER      |  REQUIRED       | TYPE |
    | -------------- | --------------- | ----- |
    | v1       | yes | `Integer` or numerical string |
    | v2       | yes | `Integer` or numerical string |
 
    Example using curl:
    
    ```sh
    $ curl -X POST \
       http://localhost:3000/collisions/add \
       -H 'content-type: application/json' \
       -d '{
         "v1": 7,
         "v2": "8"
       }'
    ```
    
    Example response:
    
    ```curl
    HTTP/1.1 201 CREATED
    Status: 201 CREATED
    ```
		
    If you don't pass any parameters in the body, you'll receive a HTTP Status Code `401` with a message saying what's wrong.

    Now, if you go to `/collisions` again, you'll see all the collisions again, but with these two vertices added to the graph. If they belongs to a collision, they will appear in the response body as the following:
 
    ```json
    [
      [1,2,3,4], 
      [5,6,7,8]
    ]
    ```


- Checking if two vertices belongs to the same collision network:
    
    | PARAMETER |  REQUIRED            | TYPE |
    | -------------- | --------------- | -----------------|
    | v1       | yes | `Integer` or numerical string |
    | v2       | yes | `Integer` or numerical string |

    Note that now you have to send the vertices in the url, like so:
    
    ```curl 
    GET /collisions/check/[vertex1]/[vertex2]
    ```
 
   Example with curl:
   
    ```sh
    $ curl -X GET \
    http://localhost:3000/collisions/check/1/2 \
    -H 'content-type: application/json'  
    ```

   If true, they belong to the same network, we'll get this network in the response body:

    ```curl
    HTTP/1.1 200 OK
    Status: 200 
    Content-Type: application/json
    [ 
        [ 1,2,3,4 ]
    ]
    ```

And that's it! ;) Thanks for the opportunity and the challenge! 


-----------------
		


### &copy; Rodolfo Costa, 2017-06