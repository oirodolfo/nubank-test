(defproject nu "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url  "http://www.eclipse.org/legal/epl-v10.html"}
  :main nu.core/init
  ;:main ^:skip-aot nu.nugraph
  :plugins [[lein-ring "0.12.0"] [lein-cljfmt "0.5.6"] [lein-kibit "0.1.5"]]
  :ring {:handler nu.core/app}
  :target-path "target/%s"
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [compojure "1.6.0"]
                 [cheshire "5.7.1"]
                 [com.taoensso/tufte "1.1.1"]
                 [com.taoensso/timbre "4.10.0"]
                 [ring "1.6.1"]
                 [ring/ring-defaults "0.3.0"]
                 [ring/ring-json "0.4.0"]
                 [prone "1.1.4"]
                 [metosin/ring-http-response "0.9.0"]
                 [prismatic/schema "1.1.6"]
                 ]
  :profiles
  {:dev {:dependencies [[javax.servlet/servlet-api "2.5"]
                        [ring/ring-mock "0.3.0"]
                        [lein-kibit "0.1.5"]]}
        :ring {:stacktrace-middleware prone.middleware/wrap-exceptions}
   })
