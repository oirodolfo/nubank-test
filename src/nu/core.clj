(ns nu.core
  (:require [compojure.core :refer :all]
            [compojure.route :as route]
            [cheshire.core :refer [generate-string]]
                     [ring.middleware.defaults :refer [wrap-defaults api-defaults]]
                     [ring.middleware.stacktrace :refer [wrap-stacktrace]]
                     [ring.util.response :refer [response created]]
                     [ring.middleware.json :refer [wrap-json-response wrap-json-params]]
                     [nu.nugraph :refer :all]
                      [nu.utils :refer [convert-to-numeric str->int]]
            ))



(defn handler-get-collisions [request]
  "Get collisions network from graph generated in nu.nugraph namespace"
  (response (generate-string (collisions-network @(:graph nu-graph)))))

(defn handler-add-vertices [params]
  "Add two vertices to the graph. First, it checks if the two numbers are numerical,
  then, give the apropriate response if these values were added to the graph"
  (if (seq? params)
    (let [numbers (convert-to-numeric params)]
      (if (= 2 (count numbers))
        (let [[v1 v2] numbers
              graph-updated (add-vertices nu-graph v1 v2)]
          (created {}))
        (response "You must provide two numbers in order the application work correctly")))
    (response "You didn't provide the two numbers in the right format. It should be written as follows: { v1: 0, v2: 1}"))
)

(defn handler-check-collisions
  "Transform "
  [params]
  (let [[n1 n2] (->> params (map #(Integer/parseInt %)))
        collisions (get-vertices-and-collisions nu-graph n1 n2)]
        (if-not (empty? collisions)
                  (response (generate-string collisions))
                  (response (generate-string {:error "Not found" :message "The nodes provided doesn't belong to any collision network."}))
                  ))
    )

(defroutes app-routes
  (context "/collisions" []
    (GET "/" [] handler-get-collisions)
    (GET "/check/:v1/:v2" [v1 v2] (handler-check-collisions [v1 v2]))
    (POST "/add" {params :params} (handler-add-vertices  (vals params)))))

(defn init [& args])

(def app
  (-> app-routes
      (wrap-json-params wrap-json-response api-defaults)))
