(ns nu.nugraph
  (:require
   [taoensso.timbre :as timbre
    :refer [log trace debug info warn error fatal report
            logf tracef debugf infof warnf errorf fatalf reportf
            spy get-env]]
   [clojure.string :as str]
   [clojure.string :as string]
   [clojure.pprint :as pprint]
   [nu.utils :refer [convert-to-numeric]]
   [cheshire.core :refer :all]
   [taoensso.tufte :as tufte :refer (defnp p profiled profile)])
  (:use (clojure set)
        (ring.middleware.params)
        (ring.util.response)
        (clojure.pprint))
  (:import (clojure.lang PersistentQueue)))

(defprotocol GraphProtocol
  (add-vertices [graph-instance v1 v2])
  (add-multiple-vertices [graph-instance coll])
  (populate-graph-from-file [graph-instance])
  (collision-network [graph-instance])
  (vertices-belongs-to-same-collision [graph-instance v1 v2])
  (get-vertices [graph-instance]))

(declare map->NewNewNewGraph nu-graph)

(defn bfs
  [successors start visited]
    (letfn [(stepfn [queue predecessors]
              (when-let [[vertex] (peek queue)]
                (cons
                  (vector vertex predecessors)
                  (lazy-seq
                    (let [neighbours (->> (successors vertex)
                                    (remove #(contains? predecessors %))
                                    (filter #((constantly true) % vertex)))]
                      (stepfn (into (pop queue) (for [neighbour neighbours] [neighbour])) (reduce #(assoc %1 %2 vertex) predecessors neighbours)))))))]
      (stepfn (conj PersistentQueue/EMPTY [start])
            (if (map? visited)
              (assoc visited start nil)
              (into {start nil} (for [v visited] [v nil]))))))


(defn vertices [graph]
  "Extract the vertices key from the graph."
  (get-in graph [:vertices]))

(defn collisions-network
  [graph]
  (let [neighbours (get-in graph [:adjacencies])]
    (first
      (reduce
        (fn [[collisions visited] vertex]
          (if (contains? visited vertex)
            [collisions visited]
            (let [[collisions-bfs predecessors-map] (reduce
                           (fn [[c] [vertex-reduced pm-reduced]]
                             [(conj c vertex-reduced) pm-reduced])
                           [[]]
                           (let [do-bfs (bfs neighbours vertex visited)]
                             do-bfs))]
              [(conj collisions collisions-bfs) predecessors-map])))
        [[] {}]
        (vertices graph)))))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(defn update-in-with-edges [graph n1 n2]
  "Takes the graph and updates its edges, vertices and adjacencies with n1 & n2."
  (-> graph
      (update-in [:edges] (fnil #(conj % [n1 n2]) #{}))
      (update-in [:vertices] (fnil #(conj % n1 n2) #{}))
      (update-in [:adjacencies n1] (fnil #(conj % n2) #{}))
      (update-in [:adjacencies n2] (fnil #(conj % n1) #{}))))



(defn update-graph-reduced
  "Checks if edges are numerical and graph exists to apply update-in-with-edges."
  [graph edges]
  (let [numbers (convert-to-numeric edges)]
    (if (= 2 (count numbers))
      (let [[n1 n2] numbers]
        (swap! (:graph graph) (fn [current]
                                (if-let [g (nil? current)]
                                  (let [g (:graph (map->NewNewNewGraph {:graph (atom {})}))]
                                    (update-in-with-edges g n1 n2))
                                  (update-in-with-edges current n1 n2))))))))

(defn update-in-with-coll
  "Instead of receiving a pair of vertex, take a vector of vertices and update
  the graph with the edges, vertices and adjacencies."
  [graph coll]
  ;(let [g (->> coll (map #(update-graph-reduced graph %)))]
  ;  (swap! (:graph graph) g)
  ;  )

  (let [graph-updated (reduce (fn [_ col] (update-graph-reduced graph col)) {} coll)]
    (swap! (:graph graph) graph-updated)
    graph-updated
    )
  )

(defn process-line [graph line callback]
  "It's called in every iteration of the lines of the file being read. It splits
  the string in a vector by the empty space, transform it into an integer and, later,
  invokes the callback function provided."
  (->> (clojure.string/split line #"\s")
       (map #(Integer/parseInt %))
       (#(callback graph %))))

(defn read-file-locally
  "This functions takes a path to a file as an argument and uses the implementation
  of java.io.Reader to proccess the whole file"
  [file]
  (clojure.java.io/reader file))

(defn get-all-nodes [graph fn-proccess-line callback]
  "Sequentially read and proccess every line of the file, receiving the graph as
  the first argument, a function to proccess each line and a callback to run after
  the line was proccessed."
  (doseq [line (with-open [rdr (read-file-locally "./resources/example.txt")] (doall (line-seq rdr)))]
    (fn-proccess-line graph line callback)))

(defn get-vertices-and-collisions
  [graph v1 v2]
  (let [collisions (collision-network graph)
        filtered (filter (fn [collision]
                           (and (some #(= v1 %) collision)
                                (some #(= v2 %) collision)
                                true))
                         collisions)]
    filtered)
  )


(defrecord NewNewNewGraph []
  GraphProtocol

  (populate-graph-from-file [graph-instance]
    (get-all-nodes graph-instance process-line update-graph-reduced)
    graph-instance)

  (add-vertices [graph-instance v1 v2]
    (update-graph-reduced graph-instance [v1 v2]))

  (add-multiple-vertices [graph-instance coll]
    (update-in-with-coll graph-instance coll))

  (collision-network [graph-instance]
    (collisions-network @(:graph graph-instance)))

  (get-vertices [graph-instance]
    (get-in graph-instance [:vertices]))

  (vertices-belongs-to-same-collision [graph v1 v2]
    (get-vertices-and-collisions graph v1 v2)
    )
  )

(def nu-graph
  "Default graph with example.txt vertices"
  (map->NewNewNewGraph {:graph (atom {})}))

(populate-graph-from-file nu-graph)

