(ns nu.utils)

(defn convert-to-numeric [params]
  (let [[value1, value2] params]
    (->> params
         (map #(.toString %))
         (map (fn [p] (try
                        (read-string p)
                        (catch Exception e))))
         (filter number?))))

(defn str->int [str]
  (if (re-matches (re-pattern "\\d+") str)
    (read-string str)))
