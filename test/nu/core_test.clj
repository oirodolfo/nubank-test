(ns nu.core-test
  (:require [clojure.test :refer :all]
            [nu.nugraph :refer :all])
  (:import (clojure.lang PersistentArrayMap PersistentVector PersistentHashSet)))

(defn empty-graph
  []
  (map->NewNewNewGraph {:graph (atom {})}))

(deftest graph-operations
  (testing "Testing basic graph operations"

    (let [e-graph (empty-graph)
          add-vertices (add-vertices e-graph 1 2)
          graph @(:graph e-graph)
          vertices (:vertices graph)
          edges (:edges graph)
          adjacencies (:adjacencies graph)
          connections (collisions-network graph)]

      (are [expected got] (= expected got)
                          (type vertices) PersistentHashSet
                          (type edges) PersistentHashSet
                          (type adjacencies) PersistentArrayMap
                          (type connections) PersistentVector

                          false (empty? vertices)
                          false (empty? edges)
                          false (empty? adjacencies)
                          false (empty? connections)

                          vertices #{1 2}
                          edges #{[1, 2]}
                          adjacencies {1 #{2}, 2 #{1}}
                          connections [[1 2]]))
    )

  (testing "Testing with multiple vertex at once."
    (let [e-graph (empty-graph)
          graph (update-in-with-coll e-graph [[1 2] [2 3] [1 4] [5 6] [6 7]])
          vertices (:vertices graph)
          edges (:edges graph)
          adjacencies (:adjacencies graph)
          connections (collisions-network graph)]


      (are [expected got] (= expected got)
                          (type vertices) PersistentHashSet
                          (type edges) PersistentHashSet
                          (type adjacencies) PersistentArrayMap
                          (type connections) PersistentVector

                          false (empty? vertices)
                          false (empty? edges)
                          false (empty? adjacencies)
                          false (empty? connections)

                          vertices #{7 1 4 6 3 2 5}
                          edges #{[2 3] [6 7] [1 4] [5 6] [1 2]}
                          adjacencies {1 #{4 2}, 2 #{1 3}, 3 #{2}, 4 #{1}, 5 #{6}, 6 #{7 5}, 7 #{6}}
                          connections [[7 6 5] [1 4 2 3]]
                          )))

  (testing "Testimg ."
    (let [v1 1
          v2 2
          e-graph (empty-graph)
          add-vertices (update-in-with-coll e-graph [[1 2] [2 3] [1 4] [5 6] [6 7]])
          graph e-graph]

      (are [expected got] (= expected got)
                          ;(get-vertices-and-collisions graph 1 2) [1 4 2 3]
                          (get-vertices-and-collisions e-graph 1 2) [1 4 2 3]
                          )
      )
    ))
